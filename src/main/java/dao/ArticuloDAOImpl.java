package dao;

import modelo.Articulo;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ArticuloDAOImpl implements GenericDAO<Articulo>{
    final String SQLSELECTALL = "select * from articulos;";
    final String SQLSELECTPK = "select * from articulos where id = ?;";
    final String SQLINSERT = "INSERT INTO empresa_ad.articulos (nombre, precio, codigo, grupo, stock) VALUES(?, ?, ?, ?, ?);";
    final String SQLUPDATE = "UPDATE empresa_ad.articulos SET nombre= ?, precio=?, codigo=?, grupo=?, stock=? WHERE id=?;";
    final String SQLDELETE = "DELETE FROM empresa_ad.articulos WHERE id= ?;";
    private final PreparedStatement pstSelectPK;
    private final PreparedStatement pstSelectAll;
    private final PreparedStatement pstInsert;
    private final PreparedStatement pstInsertKey;
    private final PreparedStatement pstUpdate;
    private final PreparedStatement pstDelete;

    public ArticuloDAOImpl() throws SQLException {
        Connection con = ConexionBD.getConexion();
        pstSelectPK = con.prepareStatement(SQLSELECTPK);
        pstSelectAll = con.prepareStatement(SQLSELECTALL);
        pstInsert = con.prepareStatement(SQLINSERT);
        pstInsertKey = con.prepareStatement(SQLINSERT, Statement.RETURN_GENERATED_KEYS);
        pstUpdate = con.prepareStatement(SQLUPDATE);
        pstDelete = con.prepareStatement(SQLDELETE);
    }

    public void cerrar() throws SQLException {
        pstSelectPK.close();
        pstSelectAll.close();
        pstInsert.close();
        pstUpdate.close();
        pstDelete.close();
    }

    @Override
    public Articulo findByPK(int id) throws Exception {
        Articulo a = null;
        pstSelectPK.setInt(1, id);
        ResultSet rs = pstSelectPK.executeQuery();
        GrupoDAOImpl grupoDAO = new GrupoDAOImpl();
        if (rs.next()) {
            a = new Articulo(id, rs.getString("nombre"), rs.getInt("precio"), rs.getString("codigo"), grupoDAO.findByPK(rs.getInt("grupo")), rs.getInt("stock"));
        }
        rs.close();
        return a;
    }

    @Override
    public List<Articulo> findAll() throws Exception {
        List<Articulo> listaArticulos = new ArrayList<Articulo>();
        ResultSet rs = pstSelectAll.executeQuery();
        GrupoDAOImpl grupoDAO = new GrupoDAOImpl();
        while (rs.next()) {
            listaArticulos.add(new Articulo(rs.getInt("id"), rs.getString("nombre"), rs.getDouble("precio"), rs.getString("codigo"), grupoDAO.findByPK(rs.getInt("grupo")), rs.getInt("stock")));
        }
        rs.close();
        return listaArticulos;
    }

    @Override
    public boolean insert(Articulo artInsert) throws Exception {
        pstInsert.setString(1, artInsert.getNombre());
        pstInsert.setDouble(2, artInsert.getPrecio());
        pstInsert.setString(3, artInsert.getCodigo());
        pstInsert.setInt(4, artInsert.getGrupo().getId());
        pstInsert.setInt(5, artInsert.getStock());
        int insertados = pstInsert.executeUpdate();
        return (insertados == 1);
    }

    public Articulo key(Articulo artInsert) throws SQLException {
        pstInsertKey.setString(1, artInsert.getNombre());
        pstInsertKey.setDouble(2, artInsert.getPrecio());
        pstInsertKey.setString(3, artInsert.getCodigo());
        pstInsertKey.setInt(4, artInsert.getGrupo().getId());
        pstInsertKey.setInt(5, artInsert.getStock());
        int insertados = pstInsertKey.executeUpdate();
        if(insertados == 1){
            ResultSet rsClaves = pstInsertKey.getGeneratedKeys();
            rsClaves.next();
            artInsert.setId(rsClaves.getInt(1));
            rsClaves.close();
            return artInsert;
        }
        return artInsert;
    }

    @Override
    public boolean update(Articulo artUpdate) throws Exception {
        pstUpdate.setString(1, artUpdate.getNombre());
        pstUpdate.setDouble(2, artUpdate.getPrecio());
        pstUpdate.setString(3, artUpdate.getCodigo());
        pstUpdate.setInt(4, artUpdate.getGrupo().getId());
        pstUpdate.setInt(5, artUpdate.getStock());
        pstUpdate.setInt(6, artUpdate.getId());
        int actualizados = pstUpdate.executeUpdate();
        return (actualizados == 1);
    }

    @Override
    public boolean delete(int id) throws Exception {
        pstDelete.setInt(1, id);
        int borrados = pstDelete.executeUpdate();
        return (borrados == 1);
    }

    @Override
    public boolean delete(Articulo artDelete) throws Exception {
        return this.delete(artDelete.getId());
    }
}

package vista;

import dao.ArticuloDAOImpl;
import dao.ConexionBD;
import dao.GrupoDAOImpl;
import modelo.Articulo;
import modelo.Grupo;

import java.sql.SQLException;
import java.util.List;

public class App {
    public static void main(String[] args) {
        try {
            ArticuloDAOImpl articuloDAO = new ArticuloDAOImpl();

            List<Articulo> articulos = articuloDAO.findAll();
            for (Articulo art : articulos) {
                System.out.println(art);
            }
            Articulo art = articuloDAO.findByPK(4);
            if (art != null) {
                System.out.println("Encontrado art 4: " + art);
            }

            // insertar
            GrupoDAOImpl grupoDAO  = new GrupoDAOImpl();
            Grupo g = grupoDAO.findByPK(1);
            Articulo artInsertar = new Articulo("monitor1", 100, "mon1", g, 4);
/*            if (articuloDAO.insert(artInsertar)) {
                System.out.println("Insertado!");
            }*/
            if(articuloDAO.key(artInsertar) != null){
                System.out.println("Insertado!" + artInsertar);
            }


            // actualizar
            Articulo artModificar = articuloDAO.findByPK(5);
            if (artModificar != null) {
                artModificar.setNombre("otro nombre");
                artModificar.setPrecio(100);
                artModificar.setCodigo("mon2");
                artModificar.setGrupo(g);
                artModificar.setStock(5);
                if (articuloDAO.update(artModificar)) {
                    System.out.println("Modificado!!");
                }
            }

            // eliminar
            if (articuloDAO.delete(397)) {
                System.out.println("Borrado!!");
            }
            Articulo artEliminar = articuloDAO.findByPK(398);
            if (artEliminar != null) {
                if (articuloDAO.delete(artEliminar)) {
                    System.out.println("Borrado!!");
                }
            }

            articuloDAO.cerrar();
            ConexionBD.cerrar();

        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
